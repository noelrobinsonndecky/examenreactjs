import React from "react";

const Dashboard = function () {
  return (
    <div className=" border p-4">
      <div>
        <p>Dashboard</p>
        <p>Thusday Julay 28 2020</p>
      </div>
      {/* secttion1 */}
      <section className="border p-2 mb-5">
        <div className="row">
          <div className="col-8">
            <p>Welcome back your dashboard is ready</p>
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Pariatur
              quas omnis incidunt illum placeat ullam libero ea , est repellat
              deserunt!
            </p>
            <button className="btn btn-info">Get started</button>
          </div>
          <div className="col-4">
            <img src="../images/data-analyst-metier.jpg" alt="metier1" />
          </div>
        </div>
      </section>

      {/* section2 */}
      <section className="border p-2 mb-5">
        <div className="row">
          <div className="col-3">
            <div className="card">
              <p>Earning</p>
              <p>$4.390</p>
              <p>12%</p>
            </div>
          </div>
          <div className="col-3">
            <div className="card">
              <p>Everage sale price</p>
              <p>$27.00</p>
              <p>12%</p>
            </div>
          </div>
          <div className="col-3">
            <div className="card">
              <p>Cicks</p>
              <p>$27.00</p>
              <p>12%</p>
            </div>
          </div>
          <div className="col-3">
            <div className="card">
              <p>Conversation</p>
              <p>1.23%</p>
              <p>1%</p>
            </div>
          </div>
        </div>
      </section>

      {/* section3 */}
      <section>
        <div className="row">
          <div className="col-4 border">
            <div className="card text-center p-3 mb-3">
              <h6>Report Generation</h6>
              <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsam
                nihil vel, modi voluptatum.
              </p>
              <div>
                <button className="btn btn-info">Continue</button>
              </div>
            </div>
            <div className="card text-center p-3">
              <p>Earning reports</p>
              <br />
              <p>earning report</p>
              <p>Everage sales price</p>
              <p>Engagement</p>
              <p>converion</p>
              <p>Segment</p>
            </div>
          </div>
          <div className="col-8 border">
            <div className="card mt-4 mb-5">
              <div>
                <h6>Revenue Summary</h6>
                <br />
                <img src="" alt="monimg" />
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <div class="card">
                  <h5 class="card-header">Sales Reporting</h5>
                  <div class="card-body">
                    <img src="" alt="sales" />
                  </div>
                </div>
              </div>
              <div className="col-6">
                <div class="card">
                  <h5 class="card-header">Traffic Sources</h5>
                  <div class="card-body">
                    <img src="" alt="sales" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Dashboard;
